import time

from langchain.callbacks.manager import CallbackManager
from langchain.callbacks.streaming_stdout import StreamingStdOutCallbackHandler
from langchain_community.llms import Ollama
from langchain.prompts import PromptTemplate
from langchain.schema import StrOutputParser
from langchain.chains import LLMChain
from langchain.schema.output_parser import BaseLLMOutputParser

model = "codellama:34b-instruct"
#model = "codellama:13b-instruct"
#model = "codellama13b-instruct-code"
times = 100

llm = Ollama(model=model,
             base_url='http://192.168.1.82:11434')

codellama_template = PromptTemplate.from_template(
    "<s>[INST] <<SYS>>\\n{system}\\n<</SYS>>\\n\\n{user}[/INST]"
)


system = """You are an expert programmer that writes simple, concise code.
Provide your answer in Python, only code, no explanation.
The answer should be a single function.
If the function uses some module, include the corresponding import line.
Function name: myfunction
"""

user = """Sort a list of dates in reverse order (newest first).
Dates are in the format DD-MM-YYYY, such as for example 25-03-2022.
Arguments: list of strings, each corresponding to a date.
Returns: sorted list of strings, each corresponding to a date.
"""

class CodeParser(StrOutputParser):

    def parse_result(self, output):
        output = output[0].text
        split_md = output.split('```', 2)
        if len(split_md) >= 3:
            # We found a markdown quote environment
            quote = split_md[1]
            try:
                code = quote.split('\n', 1)[1]
                return code
            except IndexError:
                pass
        return output


#chain = LLMChain(llm=llm, prompt=codellama_template, output_parser=CodeParser())
#chain = llm | codellama_template | CodeParser()
chain = codellama_template | llm | CodeParser()
for i in range(times):
    print("Try:", i)
    output = chain.invoke({'system': system, 'user': user})
    print(output)
