import json
import requests

"""
Program for testing ollama via its REST interface.

NOTE: ollama must be running for this to work, start the ollama app or run `ollama serve`
"""

#model = 'stablelm-zephyr'
model = 'codellama:13b-instruct'
model = "codellama:34b-instruct"
times = 100

codellama_template = "<s>[INST] <<SYS>>\\n{system}\\n<</SYS>>\\n\\n{user}[/INST]"

codellama_prompt = codellama_template.format(
    system="""You are an expert programmer that writes simple, concise code.
    Provide your answer in Python, only code, no explanation.
    The answer should be a single function.
    If the function uses some module, include the corresponding import line.
    Function name: myfunction
    """,
    user="""Sort a list of dates in reverse order (newest first).
Dates are in the format DD-MM-YYYY, such as for example 25-03-2022.
Arguments: list of strings, each corresponding to a date.
Returns: sorted list of strings, each corresponding to a date.
"""
)


def fix(response):
    split_md = response.split('```', 2)
    if len(split_md) >= 3:
        # We found a markdown quote environment
        quote = split_md[1]
        try:
            code = quote.split('\n',1)[1]
            return code, 'md'
        except IndexError:
            pass
    return response, None

def generate():
    r = requests.post('http://192.168.1.82:11434/api/generate',
                      json={
                          'model': model,
                          'prompt': codellama_prompt,
                          'context': [],
                      },
                      stream=True)
    r.raise_for_status()

    response = ""
    for line in r.iter_lines():
        body = json.loads(line)
        response_part = body.get('response', '')
        # the response streams one token at a time, print that as we receive it
        #print(response_part, end='', flush=True)
        response += response_part

        if 'error' in body:
            raise Exception(body['error'])

        if body.get('done', False):
            #return body['context']
            #return response, 'error'
            break
    code, process = fix(response)
    return code, process

def main():
    space = 0
    md = 0
    error = 0

    for i in range(times):
        code, process = generate()
        if process == 'md':
            md += 1
        elif process == 'error':
            error += 1
        elif code.isspace():
            space += 1
        print(code)
        print(i + 1, "Markdown:", md, "Space:", space)


if __name__ == "__main__":
    main()
