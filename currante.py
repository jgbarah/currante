import datetime
import logging
import subprocess
try:
    import tomllib
except ImportError:
    import tomli as tomllib
import currante_openai
import currante_codellama

from langchain_community.chat_models import ChatOllama

from langchain.prompts.chat import ChatPromptTemplate

llm = None
templates = None
get_content = None

def remove_markdown(text: str) -> str:
    output = ""
    for line in text.splitlines():
        if not line.startswith("```"):
            output += line + '\n'
    return output

def set_model(model):
    global llm, templates, get_content
    if model == "gpt3.5-turbo":
        templates = currante_openai.templates
        llm = currante_openai.llm
        get_content = currante_openai.get_content
    elif model == "codellama":
        templates = currante_openai.templates
        llm = currante_codellama.llm
        get_content = currante_codellama.get_content
    elif model == "wizardcoder":
        llm = ChatOllama(model='wizardcoder:13b-python')
    else:
        llm = None

set_model("gpt3.5-turbo")

def log(kind, content):
    timestamp = datetime.datetime.now().isoformat()
    logging.info(f"**{kind}** {timestamp}\n{content}\n**/{kind}**\n\n")

def read_specs(filenames):
    specs = []
    tests = []
    for filename in filenames:
        with open(filename, "rb") as f:
            data = tomllib.load(f)
        spec, name, arguments, returnval, testscode = (data.get('spec'),
                                            data.get('name'),
                                            data.get('arguments'),
                                            data.get('returnval'),
                                            data.get('tests', ''))
        specs.append([name, arguments, returnval, spec])
        tests.append(testscode)
    return specs, tests

def write_test(test_file, function_file, name, arguments, returnval, spec,
               previous="", comment=""):

    system_prompt = templates['tests']['system']
    human_prompt = templates['tests']['human']
    invoke_data = {'testmodule': 'unittest',
                    'functionfile': function_file,
                    'name': name,
                    'arguments': arguments,
                    'returnval': returnval,
                    'spec': spec}
    if comment != "":
        system_prompt += (
            "The user also provides a previous version of the testsuite, "
            "with some advice to change them. Follow this advice when producing "
            "a complete new explanation for the testsuite.")
        human_prompt += ("This is the previous testsuite:\n{previous}\n"
          "This is the advice to change it:\n{comment}\n")
        invoke_data.update({'previous': previous,
                         'comment': comment})

    prompt = ChatPromptTemplate.from_messages([
        ("system", system_prompt),
        ("human", human_prompt)
    ])
    log(kind='SPEC', content=spec)
    chain = prompt | llm | get_content | remove_markdown
    test = chain.invoke(invoke_data)
    log(kind='TEST', content=test)
    print(test, file=open(test_file, 'w'))
    return test


def read_test(function, function_file, test_file):
    with open(test_file, 'r') as f:
        return f.read()


def explain_test(test, spec, previous, comment):

    system_prompt = (
        "You are a programmer who writes a certain testsuite for a function. "
        "The user gives the testsuite and the specification for the function. "
        "Explain as much as possible which tests are run by the testsuite, and why. "
        "Focus on what is actually tested, describing it in terms of what is the "
        "expected output, not describing parts of the code that are not tests. "
        "Explain the testsuite in a way that a non-programmer can understand."
        "Use this format for the explanation, in Mardkown:\n"
        "* Test name: ...\n"
        "  * Input: ...\n"
        "  * Expected output: ...\n"
        "  * Explanation: ...\n"
    )
    human_prompt = (
        "This is the testsuite:\n{test}\n"
        "This is the specification of the function: {spec}.\n"
    )
    invoke_data = {'test': test, 'spec': spec}

    if comment != "":
        system_prompt += (
            "The user also provides a previous explanation, with a request about how "
            "to improve it. "
            "Produce a completely new explanation, using the request as advice "
            "to improve the previous explanation. "
        )
        human_prompt += (
            "This is the previous explanation:\n{previous}\n"
            "This is the request:\n{comment}\n"
        )
        invoke_data.update({'previous': previous, 'comment': comment})

    prompt = ChatPromptTemplate.from_messages([
        ("system", system_prompt),
        ("human", human_prompt)
    ])

    chain = prompt | llm | get_content
    answer = chain.invoke(invoke_data)
    log(kind='EXPLANATION', content=answer)
    return answer

def write_function(function_file, name, arguments, returnval, spec, test,
                   previous, advice):
    system_prompt = ("You are a programmer who writes functions. "
          "Your task is to write a function ready to be executed. "
          "The name of the function, its arguments"
          "and its return value are given by the user. "
          "The user also gives you the specification of the function, and"
          "a testsuite that the function should pass. "
          "Write ONLY the function, ready to be executed. "
          "nothing else, no explanation, no extra code. "
          "Write it in plain text, no markdown, no code blocks. "
          "Use the Python programming language.")
    human_prompt = (
            "This is the name of the function: {name}."
            "These are the arguments of the function: {arguments}.\n"
            "This is the expected return value of the function: {returnval}.\n"
            "This is the specification:\n{spec}\n"
            "This is the testsuite:\n{test}\n"
    )
    invoke_data = {'name': name,
                           'arguments': arguments,
                           'returnval': returnval,
                           'spec': spec,
                           'previous': previous,
                           'spec': spec,
                           'test': test
                           }
    if previous != "":
        system_prompt += (
            "The user also gives you a previous version of the function. "
            "The user also gives you some advice on why some of the tests failed "
            "and how to fix the function so they pass. "
            "Write the function so that failed tests pass, "
            "taking advantage of the advice.")
        human_prompt += (
            "This is the previous version of the function:\n{previous}\n"
            "This is the advice on why tests fail and how to fix the function:\n{advice}\n"
        )
        invoke_data.update({'previous': previous, 'advice': advice})

    prompt = ChatPromptTemplate.from_messages([
        ("system", system_prompt),
        ("human", human_prompt)
    ])
    chain = prompt | llm | get_content | remove_markdown
    function = chain.invoke(invoke_data)
    log(kind='FUNCTION', content=function)
    print(function, file=open(function_file, 'w'))
    return function


def read_function(function_file):
    with open(function_file, 'r') as f:
        return f.read()

def run_test(test_file):
    # Run the test and capture output
    try:
        proc = subprocess.run(['python3', '-m', 'unittest', test_file],
                              text=True, timeout=30,
                              stdout=subprocess.PIPE,
                              stderr=subprocess.PIPE)
        returncode = proc.returncode
        stdout = proc.stdout
        stderr = proc.stderr
    except subprocess.TimeoutExpired:
        returncode = 1
        stdout = "E"
        stderr = "TimeoutExpired: Command '['python3', '-m', 'unittest', 'produced_test.py']' timed out"
    log(kind="TEST STDOUT", content=stdout)
    log(kind="TEST STDERR", content=stderr)
    return returncode, stdout, stderr


def analyze_result(result, function_file, test_file):
    def find_text_line(lines, text):
        for i, line in enumerate(lines[::-1]):
            if text in line:
                return line
        return None

    returncode, stdout, stderr = result
    stderr_lines = stderr.splitlines()
    if returncode == 0:
        # Test passed
        status = "PASSED"
    elif stderr_lines[-1].startswith("FAILED"):
        status = "FAILED"
    else:
        status = "ERROR"
    log(kind="TEST RESULT", content=status)
    return status, stderr

def explain_fail(test, function, output):

    system_prompt = (
        "You are a programmer who explains why a testsuite for a function failed. "
        "The user gives the testsuite, the function being tested, "
        "and the output produced when running the testsuite. "
        "Explain which tests are failing, and why. "
        "Explain how the function could be fixed to pass the tests. "
        "Explain only the tests that fail"
        "Use this format for the explanation, in Markdown:\n"
        "* Test name: ...\n"
        "  * Why it fails: ...\n"
        "  * How to fix the function: ...\n"
    )
    human_prompt = (
        "This is the testsuite:\n{test}\n"
        "This is the function:\n{function}\n"
        "This is the output produced when running the testsuite:\n{output}.\n"
    )
    invoke_data = {'test': test, 'function': function, 'output': output}

    prompt = ChatPromptTemplate.from_messages([
        ("system", system_prompt),
        ("human", human_prompt)
    ])

    chain = prompt | llm | get_content
    explanation = chain.invoke(invoke_data)
    log(kind='TEST RUN EXPLANATION', content=explanation)
    return explanation
