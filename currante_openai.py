
from langchain_community.chat_models import ChatOpenAI

llm = ChatOpenAI()

def get_content(output):
    """Return the output from a call to invoke"""
    return output.content

templates = {
    'tests': {
        'system': ("You are a programmer who writes a testsuite for a function, which should work "
                 "according to specification given by the user. "
                 "The user provides the name of the function, "
                 "the list of arguments to the function, "
                 "and the expected return value of the function. "
                 "Write ONLY the testsuite with the tests for the function, "
                 "but not the function itself, Write all in a single unit test class. "
                 "ready to be run, nothing else, no explanation, no extra code. "
                 "Important: Write each test (each assertion) as a separate function. "
                 "Use the Python programming language, and the {testmodule} module. "
                 "The function to test is in file {functionfile}."),
        'human': ("This is the name of the function: {name},\n"
                "These are the arguments of the function: {arguments}.\n"
                "This is the expected return value of the function: {returnval}.\n"
                "This is the specification:\n{spec}\n")
    }
}
