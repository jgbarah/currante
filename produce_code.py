import argparse
import logging
import os
from statistics import mean
from timeit import default_timer as timer

from currante import (set_model, write_function, read_specs,
                      run_test, analyze_result, explain_fail)

test_file = "produced_test.py"
function_file = "produced_function.py"

def get_args():
    parser = argparse.ArgumentParser(
        prog='produce_code',
        description="Evalaute the feedback of testing information to produce code"
    )
    parser.add_argument('--nofeedback', action='store_true',
                        help="Don't feed test results back to the LLM")
    parser.add_argument('--iterations', type=int,
                        help="Number of iterations per loop")
    parser.add_argument('--loops', type=int,
                        help="Number of loops")
    args = parser.parse_args()
    return args

args = get_args()
feedback = not args.nofeedback
iterations = args.iterations
loops = args.loops

set_model('codellama')

# Prepare logging
#
logging_level = logging.DEBUG
#logging.config.dictConfig({
#    'version': 1,
#    'disable_existing_loggers': True,
#})
logfile = 'currante.log'
logging.basicConfig(filename=logfile, filemode='w+', encoding='utf-8',
                    level=logging_level, format='%(message)s')

example_files = [os.path.join('specs', f)
                 for f in os.listdir('specs') if f.endswith('.toml')]
examples, tests = read_specs(example_files)

example = examples[1]
name = example[0]
arguments = example[1]
returnval = example[2]
spec = example[3]

test = tests[1]

# Write test_file
print(test, file=open(test_file, 'w'))

previous = ""
advice = ""

pass_loops = 0
loops_data = []
start = timer()
for loop in range(loops):
    print(f"{loop}: ", end="")
    start_loop = timer()
    for i in range(iterations):
        start_iteration = timer()
        # Produce function code, write it in function_file
        function = write_function(function_file=function_file,
                                  name=name, arguments=arguments, returnval=returnval,
                                  spec=spec, test=test,
                                  previous="", advice="")

        # Run tests
        result = run_test(test_file=test_file)
        status, output = analyze_result(result=result, function_file=function_file,
                                        test_file=test_file)
        logging.info(f"Try {i}: {status}")
        if status == "PASSED":
            pass_loops += 1
            logging.info(f"Iteration lapse: {timer()-start_iteration:.3f}")
            print(f"{status[0]}", end="")
            break
        if feedback:
            advice = explain_fail(test=test, function=function, output=output)
            previous = output
        logging.info(f"Iteration lapse: {timer() - start_iteration:.3f}")
        print(f"{status[0]}", end="")
    loops_data.append({'iterations': i+1, 'result': status})
    logging.info(f"Iteration loop: {timer() - start_loop:.3f}")
    logging.info(f"Loop: {loop}, Result: {status}, iterations: {i}")
    print()

logging.info(f"Lapse: {timer() - start}")
logging.info(f"Pass loops: {pass_loops} of {iterations} iterations")
all_iterations = [loop_data['iterations'] for loop_data in loops_data]
pass_iterations = [loop_data['iterations'] for loop_data in loops_data
                    if loop_data['result'] == "PASSED"]
logging.info(f"Mean all iterations: {mean(all_iterations):.3f}")
if len(pass_iterations) > 0:
    logging.info(f"Mean passed iterations: {mean(pass_iterations):.3f}")
