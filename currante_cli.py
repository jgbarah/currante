import argparse
import logging.config
import tomllib

from currante import (write_function, write_test, explain_test, run_test,
                      read_function, read_test,
                      analyze_result, explain_fail,
                      log)

test_file = "produced_test.py"
function_file = "produced_function.py"

def produce_test(spec: str, test_file: str, function_file: str,
                 name: str, arguments: str, returnval: str) -> str:
    """Produce a testsuite for a function produced according to a specification.

    Returns a string with the testsuite, and writes it in test_file.
    :param spec: Specification
    :param test_file: Name of the test file to produce
    :param function_file: Name of the file where the function is expected to be
    :param name: Function name
    :param arguments: Function arguments (description)
    :param returnval: Value returned by the function (description)
    :return: Produced testsuite
    """

    previous_test, explanation, comment = "", "", ""

    while comment not in ("OK", "ok", "Ok"):
        if comment.startswith(("EXP", "exp", "Exp")):
            comment = comment[3:] if len(comment) > 3 else ""
        else:
            print("Let's write the testsuite")
            test = write_test(test_file=test_file, function_file=function_file,
                              name=name, arguments=arguments, returnval=returnval,
                              spec=spec, previous=previous_test, comment=comment)
            # test = read_test(function=function, function_file=function_file, test_file=test_file)
            comment = ""

        print("Let's get an explanation of the tests")
        explanation = explain_test(test=test, spec=spec, previous=explanation, comment=comment)
        log('EXPLANATION', explanation)
        print(explanation)

        comment = input(("How can I fix the tests? (write OK if no fix is needed,"
                         "start with EXP if you want another explanation): "))
        log('COMMENT', comment)
        previous_test = test

    return test

def produce_function(spec: str, test_file: str, function_file: str,
                     name: str, arguments: str, returnval: str,
                     test: str) -> tuple[str, str]:
    """Produce a function, given a testsuite and a specification.

    Returns a string with the function, and writes it in function_file.
    :param spec: Specification
    :param test_file: Name of the test file to produce
    :param function_file: Name of the file where the function is expected to be
    :param name: Function name
    :param arguments: Function arguments (description)
    :param returnval: Value returned by the function (description)
    :param test: Testsuite for the function
    :return: Result status of testing the function, and function (if PASSED)
    """

    previous_function, output, advice = "", "", ""
    status = ""
    count = 0
    # while test_result != "PASSED" and count < 5:
    while status != "PASSED":
        count += 1
        print("Let's write the function")
        function = write_function(function_file=function_file,
                                  name=name, arguments=arguments, returnval=returnval,
                                  spec=spec, test=test,
                                  previous=previous_function, advice=advice)
        # function = read_function(function_file=function_file)
        log('FUNCTION', function)

        print("Let's run the tests")
        result = run_test(test_file=test_file)
        status, output = analyze_result(result=result, function_file=function_file,
                                        test_file=test_file)
        log('STATUS', status)
        log('OUTPUT', output)

        if status == "ERROR":
            print("Code written, error when running tests!")
            exit(2)
        elif status == "FAILED":
            print("Code written, tests failed! Let's get some advice on how to fix the function")
            advice = explain_fail(test=test, function=function, output=output)
            log('ADVICE', advice)
            previous_function = function
            answer = input("Enter to continue (type EXP to get an explanation of why tests failed): ")
            if answer.startswith(("EXP", "exp", "Exp")):
                print(advice)

    return status, function

def read_toml(filename):
    with open(filename, "rb") as f:
        data = tomllib.load(f)
    return data

def set_logging(args):
    if args.log or args.logfile:
        logging.config.dictConfig({
            'version': 1,
            'disable_existing_loggers': True,
        })
        logfile = args.logfile if args.logfile else None
        logging.basicConfig(filename=logfile, encoding='utf-8', level=logging.DEBUG,
                            format='%(message)s')

def read_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-l", "--log", action='store_true',
                        help="log actions (to STDERR or logfile, if specified)")
    parser.add_argument ("--logfile", type=str,
                        help="log file")
    parser.add_argument("-c", "--config", type=str,
                        help="config file (TOML format)")
    args = parser.parse_args()
    return args

def set_specs(args):
    if args.config:
        data = read_toml(args.config)
        spec, name, arguments, returnval = (data['spec'], data['name'],
                                            data['arguments'], data['returnval'])
    else:
        spec = input("Function specification: ")
        name = input("Function name: ")
        arguments = input("Function arguments: ")
        returnval = input("Function return value: ")
    return spec, name, arguments, returnval

def main():
    args = read_args()
    set_logging(args)
    spec, name, arguments, returnval = set_specs(args)

    test = produce_test(spec=spec, test_file=test_file, function_file=function_file,
                        name=name, arguments=arguments, returnval=returnval)

    status, function = produce_function(spec=spec, test_file=test_file,
                                        function_file=function_file,
                                        name=name, arguments=arguments, returnval=returnval,
                                        test=test)

    if status == "PASSED":
        print("Code written, tests passed!")

if __name__ == "__main__":
    main()
