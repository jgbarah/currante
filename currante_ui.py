import argparse
import logging
import os.path

import gradio as gr

from currante import (write_function, write_test, explain_test, run_test,
                      read_function, read_test,
                      analyze_result, explain_fail,
                      set_model, read_specs)

test_file = "produced_test.py"
function_file = "produced_function.py"
logging_level = logging.DEBUG

# Prepare logging
#
logging.config.dictConfig({
    'version': 1,
    'disable_existing_loggers': True,
})
logfile = 'currante.log'
logging.basicConfig(filename=logfile, filemode='w+', encoding='utf-8',
                    level=logging_level, format='%(message)s')

# Load example specifications and signatures from specs directory
#
example_files = [os.path.join('specs', f)
                 for f in os.listdir('specs') if f.endswith('.toml')]
example_specs, example_tests = read_specs(example_files)

def spec_widgets():
    spec = gr.Textbox(label="Specification", placeholder="Specification of the function",
                      lines=12)
    name = gr.Textbox(label="Function name")
    arguments = gr.Textbox(label="Function arguments (in English)")
    returnval = gr.Textbox(label="Function return value (in Englsih)",
                           lines=2)
    return(spec, name, arguments, returnval)


spec, name, arguments, returnval = spec_widgets()

with gr.Blocks(theme=gr.themes.Soft()) as ui:
    # Header
    #
    with gr.Row():
        with gr.Column(scale=1):
            gr.Markdown(
                """# Currante: English as a programming language
                **Write the specification for a function, \
                and let Currante produce the code for you.** \
                """)
        with gr.Column(scale=0):
            clear_btn = gr.Button("Clear all, start afresh")
            model_ddown = gr.Dropdown(choices=['gpt3.5-turbo', 'codellama', 'wizardcoder'],
                                      value='gpt3.5-turbo',
                                      label="Select model",
                                      scale=0, container=False)
            help_chb = gr.Checkbox(label="Show help messages",
                                   value=True, scale=1)

    # Specification tab
    #
    with gr.Tab("1. Specification"):
        spec_help = gr.Markdown("""*Start in this tab, defining the function to produce: \
        its signature (name, arguments, result value) and specification. \
        When done, click on the button to produce a testsuite for it. \
        \
        You can use some ready-to-go definitions of functions from the list of examples.* \
        """)
        with gr.Accordion(
                label=("Show examples (select from them "
                        "to fill in specification and function signature)"),
                open=False):
            examples = gr.Examples(
                examples=example_specs,
                inputs=[name, arguments, returnval, spec])
        with gr.Row():
            with gr.Column():
                spec.render()
            with gr.Column():
                name.render()
                arguments.render()
                returnval.render()
                with gr.Group():
                    gr.Markdown("When the specification and signature are done, click below.")
                    test_btn = gr.Button(
                        value="Produce testsuite before moving to the Testuite tab",
                        variant='primary')
                    test_ready = gr.Markdown()

    # Testsuite tab
    #
    with gr.Tab("2. Testsuite"):
        testsuite_help = gr.Markdown("""*When the specification is ready, use this tab \
        to produce a suitable testsuite for the function. \
        Ask for a new explanation, or for a new version of the testsuite if needed. \
        When the testsuite is good enough, ask for the function. \
        It will be produced according to the specification and the testsuite.* \
        """)
        with gr.Row():
            with gr.Column():
                with gr.Group():
                    adv_explanation = gr.Textbox(label="Advice for improving the explanation",
                                         lines=4)
                    gr.Markdown(("If the explanation is not good enough, "
                                 "provide some advice to improve it, "
                                 "and click below."))
                    exp_btn = gr.Button("Produce a new explanation")
                explanation = gr.Markdown(
                    value="",
                    label="Explanation of the testsuite")
            with gr.Column():
                with gr.Group():
                    adv_testsuite = gr.Textbox(label="Advice for improving the testsuite",
                                               lines=4)
                    gr.Markdown(("If the testsuite is not good enough, "
                                 "provide some advice to improve it, and click below."))
                    test2_btn = gr.Button("Produce a new testsuite")
                with gr.Group():
                    gr.Markdown("Click below when the testsuite is good enough.")
                    function_btn = gr.Button("Produce function before moving to the Function tab",
                                             variant='primary')
                    function_ready = gr.Markdown()
                test_chb = gr.Checkbox(label="Show the code of the testsuite",
                                       value=False)
                test = gr.Code(
                            label="Testsuite (produced by Currante)", interactive=False,
                            language="python", visible=False)

    # Function tab
    #
    with gr.Tab("3. Function"):
        function_help = gr.Markdown("""*When the testsuite is ready, use this tab \
        for producing the final version of the function, which should pass the testsuite. \
        Check if the testsuite was not passed, ask for a new version of the function, \
        which will be produced following the advice, which was produced automatically \
        from the results of running the testsuite, and the specification of the function.* \
        """)
        with gr.Row():
            with gr.Column():
                function = gr.Code(label="Function", language='python', interactive=False)
            with gr.Column():
                status = gr.Label(
                    label="Status after running the testsuite",
                    value="NOT RUN")
                function_btn2 = gr.Button("Produce a new function, using the automatic advice")
                advice_chb = gr.Checkbox(label="Show the advice produced automatically to improve the function",
                                         value=True)
                output_chb = gr.Checkbox(label="Show the output of running the testsuite",
                                         value=False)
        with gr.Row():
            with gr.Column():
                advice = gr.Markdown(
                    label="Automatic advice to improve the function",
                    visible=True)
            with gr.Column():
                output = gr.Textbox(label="Output of running the testsuite",
                                interactive=False, visible=False, lines=20)


    # Visibility handlers
    #
    @help_chb.change(inputs=[help_chb], outputs=[spec_help, testsuite_help, function_help])
    def show_help(choice):
        return gr.Code(visible=choice), gr.Code(visible=choice), gr.Code(visible=choice)

    @test_chb.change(inputs=[test_chb], outputs=[test])
    def show_test(choice):
        return gr.Code(visible=choice)

    @advice_chb.change(inputs=[advice_chb], outputs=[advice])
    def show_advice(choice):
        return gr.Markdown(visible=choice)

    @output_chb.change(inputs=[output_chb], outputs=[output])
    def show_output(choice):
        return gr.Textbox(visible=choice)

    # Action handlers
    #
    @model_ddown.change(inputs=[model_ddown])
    def select_model(choice):
        set_model(choice)

    def produce_test(name, arguments, returnval, spec, test, adv_testsuite, explanation):
        test = write_test(test_file=test_file, function_file=function_file,
                          name=name, arguments=arguments, returnval=returnval,
                          spec=spec, previous=test, comment=adv_testsuite)
        explanation = explain_test(test=test, spec=spec, previous="", comment="")
        test_ready = "**<p style='text-align: center;'>Testsuite ready, check the Testsuite tab!</p>**"
        return test, explanation, test_ready

    gr.on(triggers=[test_btn.click, test2_btn.click], fn=produce_test,
          inputs=[name, arguments, returnval, spec, test, adv_testsuite, explanation],
          outputs = [test, explanation, test_ready])

    @exp_btn.click(inputs=[test, spec, explanation, adv_explanation],
                     outputs=[explanation],
                     api_name="produce_explanation")
    def produce_explanation(test, spec, explanation, adv_explanation):
        explanation = explain_test(test=test, spec=spec, previous=explanation, comment=adv_explanation)
        return explanation

    def produce_function(name, arguments, returnval, spec, test, function, advice):
        function = write_function(function_file=function_file,
                                  name=name, arguments=arguments, returnval=returnval,
                                  spec=spec, test=test,
                                  previous=function, advice=advice)
        result = run_test(test_file=test_file)
        status, output = analyze_result(result=result, function_file=function_file,
                                        test_file=test_file)
        advice = explain_fail(test=test, function=function, output=output)
        function_ready = "**<p style='text-align: center;'>Function ready, check the function tab!</p>**"
        return function, status, advice, output, function_ready

    gr.on(triggers=[function_btn.click, function_btn2.click], fn=produce_function,
          inputs=[name, arguments, returnval, spec, test, function, advice],
          outputs=[function, status, advice, output, function_ready])

    all_texts = [name, arguments, returnval, spec, explanation, adv_explanation,
                 adv_testsuite, test, advice, function]
    @clear_btn.click(inputs=all_texts, outputs=all_texts)
    def clear_all(*all_texts):
        logging.basicConfig(filename=logfile, filemode='w+', encoding='utf-8',
                            level=logging_level, format='%(message)s', force=True)
        return [""] * len(all_texts)

# Start the game
#
parser = argparse.ArgumentParser(
                    prog='currante_ui',
                    description='UI for Currante: English as a programming language')
parser.add_argument('-s', '--share',
                    help="Share access, via a url under gradio.live",
                    action='store_true')
parser.add_argument('-p', '--public',
                    help="Bind to all IP addresses, including public (non-localhost)",
                    action='store_true')

args = parser.parse_args()
server_name = "0.0.0.0" if args.public else None
ui.queue().launch(share=args.share, server_name=server_name)
